package com.example.demo;


import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(){
        return "You are " + System.getProperty("user.name") + " and you are AWESOME!!!!!";
    }

    @GetMapping("/greeting/{name}")
    public String greeting(@PathVariable String name){
        return "That " + name + " fellow sure is a particular fellow!!!!!!!!";
    }

    @RequestMapping(value = "/palindrome", method = RequestMethod.GET)
    public String palindromeChecker (@RequestParam("Query")String inputWish) {

            char[] inputArray = new char[inputWish.length()];
            char[] reverseArray = new char[inputWish.length()];
            for (int i = 0; i < inputWish.length(); i++) {
            inputArray[i] = inputWish.charAt(i);
            reverseArray[i] = inputWish.charAt(inputWish.length()-1-i);
        }

        String wordInput = inputWish.toLowerCase();

        for (int i=0; i<wordInput.length()/2; i++){
            if (inputArray[i] != reverseArray[i])
            {

                return wordInput + " is not a palindrome!";
            }
        }
        return inputWish + " IS DEFINETLY A PALINDROME!!!";


    }
}
/*        boolean status = CheckPalindrome.isPalindrome(inputWish);

        System.out.println(inputWish);

        if (status)
        {return true;}
        else
        {return false;}


    }*/




